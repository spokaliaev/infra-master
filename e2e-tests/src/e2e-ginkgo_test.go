package main

import (
	"testing"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
	"crypto/tls"
)

func TestCart(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "api service")
}

var _ = Describe("api service", func() {

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}


	Context("initially", func() {

		_, err := client.Get("http://api-service:8080")

		It("There is not error ", func() {
			Expect(err).Should(BeNil())
		})
	})

	Context("initially", func() {

		_, err := client.Get("")

		It("There is a error ", func() {
			Expect(err).ShouldNot(BeNil())
		})
	})
})