package main

import (
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	log.Println("arguments:", os.Args[1:])
	appVer := os.Args[1]
	appURL := os.Args[2]
	apiURL := os.Args[3]

	if len(appVer) != 40 || appURL == "" || apiURL == "" {
		log.Fatal("invalid arguments")
	}

	// TODO: Get rid of the code below
	// Skipping cert verification
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	resp, err := client.Get(appURL)
	if err != nil {
		log.Fatal("bad/no response from web app")
	}

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("invalid response from web app")
	}

	webAppResp := string(buf[:])
	if !strings.Contains(webAppResp, appVer) {
		log.Fatal("invalid web app version: ", appVer)
	}
	log.Println("web app version test ... passed")

	resp, err = client.Get(apiURL)
	if err != nil {
		log.Fatal("bad/no response from api service")
	}

	buf, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("invalid response from api service")
	}

	var data map[string]interface{}
	json.Unmarshal(buf, &data)
	if !strings.Contains(webAppResp, data["Version"].(string)) {
		log.Fatal("invalid api service version: ", data["Version"].(string))
	}
	log.Println("api service version test ... passed")

	log.Println("e2e test succeded!")
}
